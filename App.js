/**
 * Sample React Native App with FrankieOne SmartUI
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {SafeAreaView, StatusBar, Platform} from 'react-native';
import {WebView} from 'react-native-webview';

// set source to a url with the configuration and token available.
// For demo purpose, a local index.html is referenced
// remote url is needed otherwise to change config a
// full update of the application is needed to update configuration.
var source =
  Platform.OS === 'ios'
    ? require('./web/index.html')
    : {uri: 'file:///android_asset/index.html'};

export default class App extends React.Component {
  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        <StatusBar barStyle="dark-content" />
        <WebView
          source={source}
          originWhitelist={'["*"]'}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          scalesPageToFit={false}
          scrollEnabled={false}
          style={{
            width: '100%',
            height: '100%',
          }}
        />
      </SafeAreaView>
    );
  }
}
