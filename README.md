# README #

This is a demo app for Smart Ui in a react native application.

### How do I get set up? ###
Follow React Native CLI quickstart for your platform here:
https://reactnative.dev/docs/environment-setup

### How do I run the example application? ###

This app runs using a locally hosted html file, in demo mode.

- cd smartUiDemo
- yarn install
- if using ios, run pod install
- npx react-native start
- npx react-native run-android or npx react-native run-ios

**To use idScanVerification, a valid token needs to passed in the ffToken parameter in index.html.**

### How do I run in a new application? ###

To test in a new react native application:

- cd smartUiDemo
- npx react-native init smartUiDemo 
- npm install --save react-native-webview
- npx react-native link react-native-webview

Then update build files, and run:
- npx react-native start
- npx react-native run-android or npx react-native run-ios

### Using in an application ###

Inport WebView

import {WebView} from 'react-native-webview';

Then point webview at a page that is able to initialise the smart ui.

<WebView
    source={source}
    originWhitelist={'["*"]'}

### Updating build files for Android ###

If using idScanVerification, then camera permissions will need to be turned on.

Edit the following file:

android/app/src/main/AndroidManifest.xml

Permissions to add to the manifest include the following:

  <uses-permission android:name="android.permission.RECORD_AUDIO" />
  <uses-permission android:name="android.permission.CAMERA" />
  <uses-feature android:name="android.hardware.camera" android:required="false" />

### Updating build files for IOS ###

If using idScanVerification, then camera permissions will need to be turned on.

Add the following to the Info.plist file:

<key>NSCameraUsageDescription</key>
<string>Required for document and facial capture</string>
<key>NSMicrophoneUsageDescription</key>
<string>Required for video capture</string>

### Running on M1 MAC ###

You might have to do this for running on M1 Macs

- sudo arch -x86_64 gem install ffi
- arch -x86_64 pod install (instead of just pod install)